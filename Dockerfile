FROM centos:6

LABEL maintainer="alex.strokach@utoronto.ca"

# Set an encoding to make things work smoothly
ENV LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8

# Resolves a nasty NOKEY warning that appears when using yum
RUN rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6

# Install basic requirements
RUN yum update -y \
    && yum install -y \
        bzip2 \
        patch \
        sudo \
        tar \
        which \
        vim \
        openssl \
    && yum clean all

COPY .condarc /root/.condarc

# Install the latest Miniconda with Python 3 and update everything
RUN curl -s -L https://repo.continuum.io/miniconda/Miniconda3-4.7.12.1-Linux-x86_64.sh > miniconda.sh && \
    openssl dgst -md5 miniconda.sh | grep 81c773ff87af5cfac79ab862942ab6b3 && \
    bash miniconda.sh -b -p /opt/conda && \
    rm miniconda.sh && \
    touch /opt/conda/conda-meta/pinned && \
    mkdir -p "/opt/conda/conda-bld" && \
    export PATH="/opt/conda/bin:${PATH}" && \
    conda update --all --yes --quiet && \
    conda clean --all --yes --quiet && \
    rm -rf /opt/conda/pkgs/*

# Install conda build and deployment tools
RUN export PATH="/opt/conda/bin:${PATH}" && \
    conda install --yes --quiet conda-build anaconda-client jinja2 setuptools && \
    conda install --yes git && \
    conda clean --all --yes --quiet && \
    conda build purge-all --quiet && \
    rm -rf /opt/conda/pkgs/*

# Install docker tools
RUN export PATH="/opt/conda/bin:${PATH}" && \
    conda install --yes --quiet gosu && \
    export CONDA_GOSU_INFO=( `conda list gosu | grep gosu` ) && \
    echo "gosu ${CONDA_GOSU_INFO[1]}" >> /opt/conda/conda-meta/pinned && \
    conda install --yes --quiet tini && \
    export CONDA_TINI_INFO=( `conda list tini | grep tini` ) && \
    echo "tini ${CONDA_TINI_INFO[1]}" >> /opt/conda/conda-meta/pinned && \
    conda clean --all --yes --quiet && \
    conda build purge-all --quiet

# Set environment variables
ENV MYUSER=myuser \ 
    MYGROUP=mygroup \
    USER_ID=8002 \
    GROUP_ID=7300 \
    HOME=/home/myuser \
    LOGNAME=myuser \
    MAIL=/var/spool/mail/myuser

# Create user
RUN groupadd -g $GROUP_ID -o $MYGROUP && \
    useradd --shell /bin/bash -u $USER_ID -g $GROUP_ID -o -c "" -m $MYUSER
RUN chown -R $MYUSER:$MYGROUP /opt/conda && \
    cp /root/.condarc $HOME/.condarc && \
    chown $MYUSER:$MYGROUP $HOME/.condarc
RUN cp -R /etc/skel $HOME && \
    chown -R $MYUSER:$MYGROUP $HOME/skel && \
    (ls -A1 $HOME/skel | xargs -I {} mv -n $HOME/skel/{} $HOME) && \
    rm -Rf $HOME/skel

# Source conda init script from .bashrc
RUN echo ". /opt/conda/etc/profile.d/conda.sh" >> root/.bashrc && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> $HOME/.bashrc

# Give sudo permission for user to install system packages (user creation is postponed 
# to the entrypoint, so we can create a user with the same id as the host)
RUN echo "${MYUSER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# Add a file for users to source to activate the conda `root` environment.
# Also add a file that wraps that for use with the `ENTRYPOINT`.
COPY entrypoint_source /opt/docker/bin/entrypoint_source
COPY entrypoint /opt/docker/bin/entrypoint

# Ensure that all containers start with tini and the user selected process.
# Activate the `conda` environment `root` (and the devtoolset compiler).
# Provide a default command (`bash`), which will start if the user doesn't specify one.
ENTRYPOINT [ "/opt/conda/bin/tini", "--", "/opt/docker/bin/entrypoint" ]
CMD [ "/bin/bash" ]
